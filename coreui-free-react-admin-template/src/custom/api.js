// import FileSaver from 'file-saver'

// call Rest API
export function callAPI(url, cb, type, body, errorHandler) {
  var token = sessionStorage.getItem('token')
  errorHandler = errorHandler === undefined ? showError : errorHandler;
  if (type === "POST") {
    return fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+token,
      },
      body: body
    })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb)
    .catch(errorHandler);
  }
  else if (type === "GET"){
    return fetch(url, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer '+token,
      }
    })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
  }
  else if (type === "PUT"){
    return fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+token,
      },
      body: body
    })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
  }
  else {
    return fetch(url, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+token,
      },
    })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
  }
}

export function callAPILogin(url, body, cb) {
  return fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  .then(checkStatus)
  .then(parseJSON)
  .then(cb);
}


// export function callAPITable(url) {
//   // var token = sessionStorage.getItem('token');
//   return fetch(url, {
//     method: 'GET',
//     headers: {
//       // 'Accept': 'application/json',
//       'Content-Type': 'application/json',
//       // 'Authorization': 'Bearer ' + token
//     },
//     body: JSON.stringify(body)
//   })
//   .then(checkStatus)
//   .then(parseJSON)
//   .then(cb);
// }

// export function downloadCSV(url, cb, filename, body){
//     var token = sessionStorage.getItem('token')
//     return fetch(url, {
//       method: 'POST',
//       headers: {
//         'Accept': 'application/json, text/plain, */*',
//         'Content-Type': 'application/json',
//         'Authorization': 'Bearer '+token,
//       },
//       body: body
//     }).then(function(response) {
//       return response.blob()
//     }).then(function(blob) {
//       FileSaver.saveAs(blob, filename)
//     }).then(cb);
// }

export function logout(){
  sessionStorage.setItem('privilege','')
  sessionStorage.setItem('jwt', '')
  sessionStorage.setItem('expire', '')
  sessionStorage.setItem('personal_number','')
  sessionStorage.setItem('session', '')
  sessionStorage.setItem('status', '')
  window.location.href = '/#/login'
}

// check status response from API
export function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    sessionStorage.setItem('time_activity', new Date())
    return response;
  }else if (response.status === 401) {
    logout()
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.statusText = response.statusText;
  error.status = response.status;
  error.response = response;
  throw error;
}

// parsing json
export function parseJSON(response) {
  return response.json();
}

export function showError(error) {
  alert("Terdapat kesalahan pada backend saat pemproses request");
}
