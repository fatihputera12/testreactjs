import React from 'react';
import { Spinner } from 'reactstrap';

const Loading = props => {
    const { show ,...other } = props;

    const loading = (
      <Spinner color="primary" className="d-block mx-auto" />
    );
    const view = (
      <div {...other} />
    );

    return show ? loading : view;
  };

export default Loading;