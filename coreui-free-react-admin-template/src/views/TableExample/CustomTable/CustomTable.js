import React, { Component } from 'react';
import { Table } from 'reactstrap';
import Pagination from "react-js-pagination";

class CustomTable extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
          {/* {header table} */}
          <thead className="thead-light">
            <tr>
              <th className="text-center">No</th>
              <th className="text-center">Norek</th>
              <th className="text-center">Tipe Channel</th>
              <th className="text-center">Jenis Transactions</th>
              <th className="text-center">Status Code</th>
              <th className="text-center">Amount</th>
              <th className="text-center">Timestamp</th>
              <th className="text-center">Jenis Kartu</th>
            </tr>
          </thead>
          {/* {body table} */}
          <tbody>
            { this.props.data.data.map((row, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{row.norek}</td>
                  <td>{row.tipe_channel}</td>
                  <td>{row.jenis_trx}</td>
                  <td>{row.status_code}</td>
                  <td>{row.trx_amount}</td>
                  <td>{row.timestamp}</td>
                  <td>{row.jenis_kartu}</td>
                </tr>
                )
              })
            }
          </tbody>
        </Table>
        <br />  
        <Pagination
          activePage={1}
          itemsCountPerPage={10}
          totalItemsCount={450}
          pageRangeDisplayed={5}
        />      
      </div>
    );
  }
}

export default CustomTable;
