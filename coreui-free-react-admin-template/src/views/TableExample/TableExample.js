import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, Table } from 'reactstrap';
import Loading from '../../custom/Loading';
import { callAPITable } from '../../custom/api.js';
import Pagination from "react-js-pagination";

class TableExample extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataTable: {
        data: [],
        totalRows: 0,
      },
      activePage: 1,
      isLoadingData: false
    }

    this.loadDataTable = this.loadDataTable.bind(this);
  }

  componentDidMount() {
    this.loadDataTable(1);  
  }

  loadDataTable(pageNumber) {
    this.setState({
      isLoadingData: true,
      activePage: pageNumber,
    });

    const body = {
      limit: 5,
      page: pageNumber,
    }

    callAPITable('http://localhost:5000/table/get-data-table', (response) => {
      this.setState({
        dataTable: response,
        isLoadingData: false,
      });
    }, body);
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Table Pagination
              </CardHeader>
              <CardBody>
                <Loading show={this.state.isLoadingData}>
                  <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                    <thead className="thead-light">
                      <tr>
                        <th>No</th>
                        <th>name</th>
                        <th>role</th>
                        <th>username</th>
                      </tr>
                    </thead>
                    <tbody>
                      { this.state.dataTable.data.map((row, index) => {
                        return (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{row.name}</td>
                            <td>{row.role}</td>
                            <td>{row.username}</td>
                          </tr>
                          )
                        })
                      }
                    </tbody>
                  </Table>
                  <Pagination 
                    itemClass="page-item"
                    linkClass="page-link"
                    activePage={this.state.activePage}
                    itemsCountPerPage={5}
                    totalItemsCount={this.state.dataTable.totalRows}
                    onChange={this.loadDataTable}
                  />      
                </Loading>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default TableExample;
