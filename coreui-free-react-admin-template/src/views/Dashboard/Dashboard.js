import React, { Component } from 'react';
import Loading from '../../custom/Loading';

class Dashboard extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <Loading show={false}>
          Dashboard
        </Loading>
      </div>
    );
  }
}

export default Dashboard;
