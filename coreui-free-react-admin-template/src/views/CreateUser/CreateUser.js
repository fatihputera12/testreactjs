import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, Button, FormGroup, Label, Input, Alert } from 'reactstrap';
import Loading from '../../custom/Loading';

class CreateUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
        formUser: {
            username: '',
            name: '',
            // role_id: '',
            password: '',
            // confirm_password: '',
        },
        lovRoles: [],
        isLoadingCreate: false,
        isConfirmPasswordError: false,
        isCreateSuccess: false,
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  componentDidMount() {
    // this.loadLovRole();
  }

  onChangeHandler(event) {
    let formUser = {...this.state.formUser};
    formUser[event.target.name] = event.target.value;

    this.setState({
        formUser,
    });
  }

  createUser() {
    this.setState({
        isConfirmPasswordError: false,
        isCreateSuccess: false,
        isLoadingCreate: true,
    });

    // if (this.state.formUser.password === this.state.formUser.confirm_password) {
        fetch('http://localhost:5000/user/insert', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.formUser)
        }).then((response) => {
            return response.json();
        })
        .then((data) => {
            this.setState({
              formUser: {
                  username: '',
                  name: '',
                  role_id: '',
                  password: '',
                  confirm_password: '',
              },
              isLoadingCreate: false,
              isCreateSuccess: true,
            });
        });    
    // } else {
    //     this.setState({
    //         isLoadingCreate: false,
    //         isConfirmPasswordError: true,
    //     });
    // }  
  }


  back(){
    const path = "/user";
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        <b>Create User</b>
                    </CardHeader>
                    <CardBody>
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="username">Username</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="text" id="username" name="username" placeholder="Username"  onChange={this.onChangeHandler} value={this.state.formUser.username} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="name">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="text" id="name" name="name" placeholder="Name" onChange={this.onChangeHandler} value={this.state.formUser.name} />
                            </Col>
                        </FormGroup>
                        {/* <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="role">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="select" name="role_id" id="role" onChange={this.onChangeHandler} value={this.state.formUser.role}  >
                                    <option value=''>Please Select Role</option>
                                { this.state.lovRoles.map((item, index) => {
                                    return (
                                        <option key={index} value={item.key}>{item.value}</option>
                                    )}
                                    )
                                }
                                </Input>
                            </Col>
                        </FormGroup> */}
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="password">Password</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="password" id="password" name="password" placeholder="Password" onChange={this.onChangeHandler} value={this.state.formUser.password} />
                            </Col>
                        </FormGroup>
                        {/* <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="confirm-password">Confirm Password</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="password" id="confirm-password" name="confirm_password" placeholder="Confirm Password" onChange={this.onChangeHandler} value={this.state.formUser.confirm_password} />
                            </Col>
                        </FormGroup> */}
                        <Row>
                            <Col>
                                <Button color="success" className="d-block mx-auto" onClick={() => this.createUser()}><i className="fa fa-plus mr-2"></i>Create</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button color="warning" onClick={() => this.back()}><i className="fa fa-arrow-left mr-2"></i>Back</Button>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col>                    
                                <Loading show={this.state.isLoadingCreate} />
                                <Alert color="danger" isOpen={this.state.isConfirmPasswordError}>
                                    Password yang Anda Masukan Tidak Sama
                                </Alert>             
                                <Alert color="success" isOpen={this.state.isCreateSuccess}>
                                    User berhasil di buat
                                </Alert>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
        </Row>
      </div>
    );
  }
}

export default CreateUser;
