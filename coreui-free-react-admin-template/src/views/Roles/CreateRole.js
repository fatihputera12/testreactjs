import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, Button, FormGroup, Label, Input, Alert } from 'reactstrap';
import Loading from '../../custom/Loading';

class CreateRole extends Component {
  constructor(props) {
    super(props);

    this.state = {
        formRole: {
            role_name: '',
            description: '',
        },
        isLoadingCreate: false,
        isCreateSuccess: false,
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  onChangeHandler(event) {
    let formRole = {...this.state.formRole};
    formRole[event.target.name] = event.target.value;

    this.setState({
        formRole,
    });
  }

  createRole() {
    this.setState({
        isCreateSuccess: false,
        isLoadingCreate: true,
    });

    fetch('http://localhost:5000/role', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.state.formRole)
    }).then((response) => {
        return response.json();
    })
    .then((data) => {
        this.setState({
            formRole: {
                role_name: '',
                description: '',
            },
            isLoadingCreate: false,
            isCreateSuccess: true,
        });
    });    
  }

  back(){
    const path = "/roles";
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        <b>Create Role</b>
                    </CardHeader>
                    <CardBody>
                        <FormGroup row>
                            <Col xs="3">
                                <Label htmlFor="role-name">Role Name</Label>
                            </Col>
                            <Col xs="9">
                                <Input type="text" id="role-name" name="role_name" placeholder="Role Name" onChange={this.onChangeHandler} value={this.state.formRole.role_name} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col xs="3">
                                <Label htmlFor="description">Description</Label>
                            </Col>
                            <Col xs="9">
                                <Input type="textarea" name="description" id="description" rows="9" placeholder="Description..." onChange={this.onChangeHandler} value={this.state.formRole.description} />
                            </Col>
                        </FormGroup>
                        <Row>
                            <Col>
                                <Button color="success" className="d-block mx-auto" onClick={() => this.createRole()}><i className="fa fa-plus mr-2"></i>Create</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button color="warning" onClick={() => this.back()}><i className="fa fa-arrow-left mr-2"></i>Back</Button>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col>                    
                                <Loading show={this.state.isLoadingCreate} />
                                <Alert color="success" isOpen={this.state.isCreateSuccess}>
                                    Role berhasil di buat
                                </Alert>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
        </Row>
      </div>
    );
  }
}

export default CreateRole;
