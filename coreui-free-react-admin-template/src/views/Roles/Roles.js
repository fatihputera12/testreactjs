import React, { Component } from 'react';
import Loading from '../../custom/Loading';
import { Row, Col, Card, CardHeader, CardBody, Button, Table } from 'reactstrap';

class Roles extends Component {
  constructor(props) {
      super(props);

      this.state = {
          dataTableRoles: [],
          isLoadingDataTableRoles: false,
      }
  }

  componentDidMount() {
    this.loadDataRoles();
  }

  loadDataRoles() {
    this.setState({
        isLoadingDataTableRoles: true
    });
    fetch('http://localhost:5000/role', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({
          dataTableRoles: data,
          isLoadingDataTableRoles: false
        });
      });  
  }

  createRole(){
    const path = "/create-role";
    this.props.history.push(path);
  }

  updateRole(id){
    const path = "/update-role/" + id;
    this.props.history.push(path);
  }

  deleteRole(id) {
    fetch('http://localhost:5000/role/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
          this.loadDataRoles();
      }); 
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        <b>Roles</b>
                    </CardHeader>
                    <CardBody>
                        <Loading show={this.state.isLoadingDataTableRoles}>
                            <Table responsive bordered hover className="table-outline mb-2 d-none d-sm-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Role Name</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                           
                                { this.state.dataTableRoles.map((row, index) => {
                                    return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{row.role_name}</td>
                                        <td>{row.description}</td>
                                        <td>
                                            <Button color="info" size="sm" className="mr-2 text-white" onClick={() => this.updateRole(row.id)}><i className="fa fa-pencil"></i></Button>                                        
                                            <Button color="danger" size="sm" onClick={() => this.deleteRole(row.id)}><i className="fa fa-trash"></i></Button>
                                        </td>
                                    </tr>
                                    )
                                    })
                                }
                                </tbody>
                            </Table>
                        </Loading>
                        <br />
                        <Row>
                            <Col>
                                <Button color="success" onClick={() => this.createRole()}><i className="fa fa-plus mr-2"></i>Create</Button>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
        </Row>
      </div>
    );
  }
}

export default Roles;
