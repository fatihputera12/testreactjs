import React, { Component } from 'react';
import Loading from '../../custom/Loading';
import { Row, Col, Card, CardHeader, CardBody, Button, ButtonGroup } from 'reactstrap';
import { Pie} from 'react-chartjs-2';

const pie = {
    labels: [],
    datasets: [
      {
        data: [],
        backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56',
          '#4dbd74',
          '#6610f2',
        ],
        hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56',
          '#4dbd74',
          '#6610f2',
        ],
      }],
  };


class DashboardNasabah extends Component {
  constructor(props) {
    super(props);

    this.state = {
        topFiveFrequencyNasabah: pie,
        isLoadingTopFiveFrequencyNasabah: false,
        stateTopFiveFrequencyNasabah: 'ALL',
        topFiveAmountNasabah: pie,
        isLoadingTopFiveAmountNasabah: false,
        stateTopFiveAmountNasabah: 'ALL',
    };

    this.changeFrequencyByTipeChannel = this.changeFrequencyByTipeChannel.bind(this);
    this.changeAmountByTipeChannel = this.changeAmountByTipeChannel.bind(this);
  }

  componentDidMount() {
      this.loadDataTopFiveFrequencyNasabah();
      this.loadDataTopFiveAmountNasabah();
  }

  loadDataTopFiveFrequencyNasabah(type) {
    this.setState({isLoadingTopFiveFrequencyNasabah: true});
    const channel = (type && type !== 'ALL') ? ('/'+type) : '';
    fetch('http://localhost:5000/dashboard-nasabah/top-five-frekuensi' + channel, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
          let  pieFreq = {
            labels: [],
            datasets: [
              {
                data: [],
                backgroundColor: [
                  '#FF6384',
                  '#36A2EB',
                  '#FFCE56',
                  '#4dbd74',
                  '#6610f2',
                ],
                hoverBackgroundColor: [
                  '#FF6384',
                  '#36A2EB',
                  '#FFCE56',
                  '#4dbd74',
                  '#6610f2',
                ],
              }],
          };

          data.forEach(item => {
            pieFreq.labels.push(item.norek);
            pieFreq.datasets[0].data.push(item.frekuensi);
          });

          this.setState({
            topFiveFrequencyNasabah: pieFreq,
            isLoadingTopFiveFrequencyNasabah: false,
          });
      }); 
  }

  loadDataTopFiveAmountNasabah(type) {
    this.setState({isLoadingTopFiveAmountNasabah: true});
    const channel = (type && type !== 'ALL') ? ('/'+type) : '';
    fetch('http://localhost:5000/dashboard-nasabah/top-five-amount' + channel, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
          let  pieAmount = {
            labels: [
            ],
            datasets: [
              {
                data: [],
                backgroundColor: [
                  '#FF6384',
                  '#36A2EB',
                  '#FFCE56',
                  '#4dbd74',
                  '#6610f2',
                ],
                hoverBackgroundColor: [
                  '#FF6384',
                  '#36A2EB',
                  '#FFCE56',
                  '#4dbd74',
                  '#6610f2',
                ],
              }],
          };

          data.forEach(item => {
            pieAmount.labels.push(item.norek);
            pieAmount.datasets[0].data.push(item.amount);
          });

          this.setState({
            topFiveAmountNasabah: pieAmount,
            isLoadingTopFiveAmountNasabah: false,
          });
      }); 
  }

  changeFrequencyByTipeChannel(type) {
    this.setState({
        stateTopFiveFrequencyNasabah: type,
    });
    this.loadDataTopFiveFrequencyNasabah(type);
  }

  changeAmountByTipeChannel(type) {
    this.setState({
        stateTopFiveAmountNasabah: type,
    });
    this.loadDataTopFiveAmountNasabah(type);
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        Dashboard Nasabah
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col xs="6">
                                <Row>
                                    <Col>
                                        <h4 className="text-center">Top 5 Frequency</h4>
                                    </Col>
                                </Row>
                                <br />
                                <Loading show={this.state.isLoadingTopFiveFrequencyNasabah}>
                                    <Row>
                                        <Col>
                                            <div className="chart-wrapper">
                                                <Pie data={this.state.topFiveFrequencyNasabah} />
                                            </div>
                                        </Col>
                                    </Row>
                                    <br />
                                    <Row>
                                        <Col>
                                            <ButtonGroup>
                                                <Button outline active={(this.state.stateTopFiveFrequencyNasabah === 'EDC')} color="primary" onClick={() => this.changeFrequencyByTipeChannel('EDC')}>EDC</Button>
                                                <Button outline active={(this.state.stateTopFiveFrequencyNasabah === 'SMS BANKING')} color="primary" onClick={() => this.changeFrequencyByTipeChannel('SMS BANKING')}>SMS BANKING</Button>
                                                <Button outline active={(this.state.stateTopFiveFrequencyNasabah === 'ATM')} color="primary" onClick={() => this.changeFrequencyByTipeChannel('ATM')}>ATM</Button>
                                                <Button outline active={(this.state.stateTopFiveFrequencyNasabah === 'INTERNET BANKING')} color="primary" onClick={() => this.changeFrequencyByTipeChannel('INTERNET BANKING')}>INTERNET BANKING</Button>
                                                <Button outline active={(this.state.stateTopFiveFrequencyNasabah === 'ALL')} color="primary" onClick={() => this.changeFrequencyByTipeChannel('ALL')}>ALL</Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                </Loading>
                            </Col>
                            <Col xs="6">
                                <Row>
                                    <Col>
                                        <h4 className="text-center">Top 5 Amount</h4>
                                    </Col>
                                </Row>
                                <br />
                                <Loading show={this.state.isLoadingTopFiveAmountNasabah}>
                                    <Row>
                                        <Col>
                                            <div className="chart-wrapper">
                                                <Pie data={this.state.topFiveAmountNasabah} />
                                            </div>
                                        </Col>
                                    </Row>
                                    <br />
                                    <Row>
                                        <Col>
                                            <ButtonGroup className="float-right">
                                                <Button outline active={(this.state.stateTopFiveAmountNasabah === 'EDC')} color="primary" onClick={() => this.changeAmountByTipeChannel('EDC')}>EDC</Button>
                                                <Button outline active={(this.state.stateTopFiveAmountNasabah === 'SMS BANKING')} color="primary" onClick={() => this.changeAmountByTipeChannel('SMS BANKING')}>SMS BANKING</Button>
                                                <Button outline active={(this.state.stateTopFiveAmountNasabah === 'ATM')} color="primary" onClick={() => this.changeAmountByTipeChannel('ATM')}>ATM</Button>
                                                <Button outline active={(this.state.stateTopFiveAmountNasabah === 'INTERNET BANKING')} color="primary" onClick={() => this.changeAmountByTipeChannel('INTERNET BANKING')}>INTERNET BANKING</Button>
                                                <Button outline active={(this.state.stateTopFiveAmountNasabah === 'ALL')} color="primary" onClick={() => this.changeAmountByTipeChannel('ALL')}>ALL</Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                </Loading>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
        </Row>
      </div>
    );
  }
}

export default DashboardNasabah;
