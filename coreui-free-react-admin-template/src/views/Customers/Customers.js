import React, { Component } from 'react';
import Loading from '../../custom/Loading';
import { Row, Col, Card, CardBody, Button, Label, Input, FormGroup, Table, Badge } from 'reactstrap';
import { callAPI } from '../../custom/api';
import Moment from 'moment';

class Customers extends Component {
  constructor(props) {
    super(props);

    this.state = {
        formRequestCustomerTransaction: {
            norek: '',
            start_date: '',
            end_date: '',
        },
        isLoadingRequest: false,
        historicalCustomerTransaction: {
            data: [],
            totalRows: 0,
        },
        isLoadingHistoricalCustomerTransaction: false,
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.request = this.request.bind(this);
  }

  componentDidMount() {
      this.loadDataHistoricalCustomerTransaction();
  }

  onChangeHandler(event) {
    let formRequestCustomerTransaction = {...this.state.formRequestCustomerTransaction};
    formRequestCustomerTransaction[event.target.name] = event.target.value;
    
    this.setState({
        formRequestCustomerTransaction,
    });
  }

  request() {
    this.setState({isLoadingRequest: true});
    callAPI('http://localhost:5000/customer-profile', (response) => {
        
    }, 'POST', JSON.stringify(this.state.formRequestCustomerTransaction))
    .then(() => {
        this.setState({
            isLoadingRequest: false
        });
        this.loadDataHistoricalCustomerTransaction();
    });
  }

  loadDataHistoricalCustomerTransaction() {
    this.setState({isLoadingHistoricalCustomerTransaction: true});
    callAPI('http://localhost:5000/customer-profile', (response) => {
        let historicalCustomerTransaction = {...this.state.historicalCustomerTransaction}; 
        historicalCustomerTransaction['data'] = response;

        this.setState({
            historicalCustomerTransaction,
        });
    }, 'GET')
    .then(() => {
        this.setState({
            isLoadingHistoricalCustomerTransaction: false
        });
    });
  }

  visualize(id){
    const path = "/customer-information/"+id;
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="animated fadeIn">
          <Row>
            <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Filter</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            <Col xs="2">
                                <Label>Account Number </Label>
                            </Col>
                            <Col xs="10">
                                <FormGroup>
                                <Input type="text" placeholder="Account Number" name="norek" onChange={this.onChangeHandler} required />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="2">
                                <Label>Time Periode </Label>
                            </Col>
                            <Col xs="5">
                                <FormGroup>
                                <Label htmlFor="startDate">Start Date</Label>
                                <Input type="date" id="startDate" placeholder="date"  name="start_date" onChange={this.onChangeHandler} />
                                </FormGroup>
                            </Col>
                            <Col xs="5">
                                <FormGroup>
                                <Label htmlFor="endDate">End Date</Label>
                                <Input type="date" id="endDate" name="end_date" placeholder="date" onChange={this.onChangeHandler} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            <Col>
                                <Button color="primary" className="d-block mx-auto" onClick={this.request} ><i className="fa fa-send mr-1"></i> Request</Button>
                            </Col>
                        </Row>
                        <br />
                        <Loading show={this.state.isLoadingRequest} />
                    </CardBody>
                </Card>
            </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Horizontal Visualize Customer Transaction</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Loading show={this.state.isLoadingHistoricalCustomerTransaction} >
                            <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">No</th>
                                        <th className="text-center">Visualize Date</th>
                                        <th className="text-center">Account Number</th>
                                        <th className="text-center">Start Date</th>
                                        <th className="text-center">End Date</th>
                                        <th className="text-center">Status</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>         
                                    { this.state.historicalCustomerTransaction.data.map((row, index) => {
                                        return (
                                        <tr key={row.id}>
                                            <td className="text-center">{index + 1}</td>
                                            <td className="text-center">
                                            <div>2018-01-01</div>
                                                <div className="small text-muted">
                                                    <span>Time</span> 00:00:00
                                                </div>
                                            </td>
                                            <td className="text-center">{row.norek}</td>
                                            <td className="text-center">{Moment(row.start_date).format('DD-MM-YYYY')}</td>
                                            <td className="text-center">{Moment(row.end_date).format('DD-MM-YYYY')}</td>
                                            <td className="text-center"><Badge color="primary">{row.status}</Badge></td>
                                            <td><Button color="primary" onClick={() => this.visualize(row.id)}><i className="fa fa-bar-chart mr-1"></i> Visualize</Button></td>
                                        </tr>
                                        )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
          </Row>
      </div>
    );
  }
}

export default Customers;
