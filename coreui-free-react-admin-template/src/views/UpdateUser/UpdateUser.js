import React, { Component } from 'react';
import { Row, Col, Card, CardBody, CardHeader, Button, FormGroup, Label, Input, Alert } from 'reactstrap';
import Loading from '../../custom/Loading';

class UpdateUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
        id: this.props.match.params.id ? this.props.match.params.id : '',
        formUser: {
            username: '',
            name: '',
            role_id: '',
        },
        lovRoles: [],
        isLoadingUpdate: false,
        isUpdateSuccess: false,
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  componentDidMount() {
    this.loadDataUsers();
    this.loadLovRole();
  }

  loadDataUsers() {
    fetch('http://localhost:5000/users/' + this.state.id, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({
            formUser: data,
        });
      });  
  }

  updateUser() {
    this.setState({
        isUpdateSuccess: false,
        isLoadingUpdate: true,
    });
    fetch('http://localhost:5000/users/' + this.state.id, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.state.formUser)
    }).then((response) => {
        return response.json();
    })
    .then((data) => {
      this.setState({
        isLoadingUpdate: false,
        isUpdateSuccess: true,
      });
    });    
  }

  loadLovRole() {
    fetch('http://localhost:5000/role/lov', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
    }).then((response) => {
        return response.json();
    })
    .then((data) => {
        this.setState({
            lovRoles: data,
        });
    });
  }

  onChangeHandler(event) {
    let formUser = {...this.state.formUser};
    formUser[event.target.name] = event.target.value;

    this.setState({
        formUser,
    });
  }

  back(){
    const path = "/user";
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        <b>Update User</b>
                    </CardHeader>
                    <CardBody>
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="username">Username</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="text" id="username" name="username" placeholder="Username" onChange={this.onChangeHandler} value={this.state.formUser.username} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="name">Name</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="text" id="name" name="name" placeholder="Name" onChange={this.onChangeHandler} value={this.state.formUser.name} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col md="3">
                                <Label htmlFor="role">Role</Label>
                            </Col>
                            <Col xs="12" md="9">
                                <Input type="select" name="role_id" id="role" onChange={this.onChangeHandler} value={this.state.formUser.role_id} >
                                { this.state.lovRoles.map((item, index) => {
                                    return (
                                        <option key={index} value={item.key}>{item.value}</option>
                                    )}
                                    )
                                }
                                </Input>
                            </Col>
                        </FormGroup>
                        <Row>
                            <Col>
                                <Button color="info" className="d-block mx-auto" onClick={() => this.updateUser()}><i className="fa fa-repeat mr-2"></i>Update</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button color="warning" onClick={() => this.back()}><i className="fa fa-arrow-left mr-2"></i>Back</Button>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col>                    
                                <Loading show={this.state.isLoadingUpdate} />
                                <Alert color="success" isOpen={this.state.isUpdateSuccess}>
                                    User berhasil di ubah
                                </Alert>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
        </Row>
      </div>
    );
  }
}

export default UpdateUser;
