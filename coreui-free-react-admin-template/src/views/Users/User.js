import React, { Component } from 'react';
import Loading from '../../custom/Loading';
import { Row, Col, Card, CardHeader, CardBody, Button, Table } from 'reactstrap';
import Pagination from "react-js-pagination";
import { Modal, ModalHeader, ModalBody, ModalFooter, Tooltip } from 'reactstrap';
import { Link } from 'react-router-dom'


class User extends Component {
  constructor(props) {
      super(props);

      this.state = {
          dataTableUsers: [],
          isLoadingDataTableUsers: false,
          ismodal:false
      }
  }
  

  componentDidMount() {
    this.loadDataUsers();
  }

  loadDataUsers() {
    this.setState({
        isLoadingDataTableUsers: true
    });
    fetch('http://localhost:5000/user/', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
        // console.log(data);
        this.setState({
          
          dataTableUsers: data["data"],
          isLoadingDataTableUsers: false
        });
      });  
  }

  createUser(){
    const path = "/create-user";
    this.props.history.push(path);
  }
  modal(){
    
    this.setState({
      ismodal: true
  });
  }
  modalclose(){
    
    this.setState({
      ismodal: false
  });
  }
  updateUser(id){
    const path = "/update-user/" + id;
    this.props.history.push(path);
  }

  deleteUser(id) {
    fetch('http://localhost:5000/user/delete/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: null
      }).then((response) => {
        return response.json();
      }).then((data) => {
          this.loadDataUsers();
      }); 
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        <b>Users</b>
                    </CardHeader>
                    <CardBody>
                    <Row>
                          <Col>
                                <Button color="success" onClick={() => this.createUser()}><i className="fa fa-plus mr-2"></i>Create</Button>
                                <Button color="danger" onClick={() => this.modal()}><i className="fa fa-trash mr-2"></i>Delete</Button>
                                {/* <button onClick={openModal}>Delete</button> */}
                            </Col>
                        </Row>
                        <br/>
                        <Loading show={this.state.isLoadingDataTableUsers}>
                            <Table responsive bordered hover className="table-outline mb-2 d-none d-sm-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>Name</th>
                                        <th>Password</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                           
                                { this.state.dataTableUsers.map((row, index) => {
                                    return (
                                    <tr key={index}>
                                        <td>{row.id}</td>
                                        <td>{row.username}</td>
                                        <td>{row.name}</td>
                                        <td>{row.password}</td>
                                        <td>
                                            {/* <Button color="info" size="sm" className="mr-2 text-white" onClick={() => this.updateUser(row.id)}><i className="fa fa-pencil"></i></Button>                                         */}
                                            <Button color="danger" size="sm" onClick={() => this.deleteUser(row.id)}><i className="fa fa-trash"></i></Button>
                                        </td>
                                    </tr>
                                    )
                                    })
                                }
                                </tbody>
                            </Table>
                            {/* <Pagination
                            activePage={1}
                            itemsCountPerPage={10}
                            totalItemsCount={450}
                            pageRangeDisplayed={5}
                      />       */}
                        </Loading>
                        <br />
                        
                    </CardBody>
                </Card>
            </Col>
        </Row>

        <Modal isOpen={this.state.ismodal} className={'modal-sm ' + this.props.className}>
          <ModalHeader>Delete</ModalHeader>
          <ModalBody>
            
          </ModalBody>
          <ModalFooter>
          <Button color="danger" onClick={() => this.modalclose()}>close</Button>
          </ModalFooter>
        </Modal>

        {/* <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
 
          <h2 ref={_subtitle => (subtitle = _subtitle)}>Hello</h2>
          <button onClick={closeModal}>close</button>
          <div>I am a modal</div>
          <form>
            <input />
            <button>tab navigation</button>
            <button>stays</button>
            <button>inside</button>
            <button>the modal</button>
          </form>
        </Modal> */}
      </div>
    );
  }
}

export default User;
