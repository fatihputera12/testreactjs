import React, { Component } from 'react';
import Loading from '../../custom/Loading';
import { Row, Col, Card, CardBody, Button, Table } from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Bar, Pie, Scatter } from 'react-chartjs-2';
import { callAPI } from '../../custom/api';
import Moment from 'moment';

// var CurrencyFormat = require('react-currency-format')
// import 'chartjs-plugin-datalabels'
var moment = require('moment')
var momentDurationFormatSetup = require("moment-duration-format")
momentDurationFormatSetup(moment)

const clustering_transaction_tunai = {
    labels: ['Clustering Transaction Tunai'],
    datasets: [
      {
        label: 'INTERNET BANKING',
        backgroundColor: '#38f5be',
        borderColor: '#38f5be',
        borderWidth: 1,
        hoverBackgroundColor: '#38f5be',
        hoverBorderColor: '#38f5be',
        data: [
            { x: 5000, y: 1000 },
            { x: 5000, y: 500 },    
        ],
      },
      {
        label: 'SMS BANKING',
        backgroundColor: '#997af8',
        borderColor: '#997af8',
        borderWidth: 1,
        hoverBackgroundColor: '#997af8',
        hoverBorderColor: '#997af8',
        data: [],
      },
      {
        label: 'EDC',
        backgroundColor: '#293249',
        borderColor: '#293249',
        borderWidth: 1,
        hoverBackgroundColor: '#293249',
        hoverBorderColor: '#293249',
        data: [],
      },
      {
        label: 'ATM',
        backgroundColor: '#f47f42',
        borderColor: '#f47f42',
        borderWidth: 1,
        hoverBackgroundColor: '#f47f42',
        hoverBorderColor: '#f47f42',
        data: [],
      }
    ]
  }

const bar = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Frequency',
        backgroundColor: 'rgba(32, 168, 216,0.2)',
        borderColor: 'rgba(32, 168, 216,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(32, 168, 216,0.4)',
        hoverBorderColor: 'rgba(32, 168, 216,1)',
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
  };

const options = {
    tooltips: {
     enabled: false,
     custom: CustomTooltips
    },
    maintainAspectRatio: false,
    plugins: {
     datalabels: { display: true, color: 'white', }
    },
    scales: {
      yAxes: [{ scaleLabel: { display: true, labelString: 'Frequency' } }],
      xAxes: [{ scaleLabel: { display: true, labelString: 'Amount' } }],
    }
  }

const pie = {
    labels: [
      'Red',
      'Green',
      'Yellow',
    ],
    datasets: [
      {
        data: [300, 50, 100],
        backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56',
        ],
        hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56',
        ],
      }],
  };
  
 var clustering_transaction = {
    maintainAspectRatio: false,
    plugins: {
     datalabels: { display: false, color: 'white', }
    },
    scales: {
      xAxes: [{
          ticks: {
              userCallback: function(label) {
                  return moment.duration(label, "seconds").format("*hh:mm", { trunc: true });
              }
           }, scaleLabel: { display: true, labelString: 'Time' }
      }],
      yAxes: [{
          ticks: {
            userCallback: function(label) {
                return "Rp. "+label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            }
         }, scaleLabel: { display: true, labelString: 'Amount' } }],
   },
   tooltips: {
    callbacks: {
       label: function(tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';
          return label + ' (' + moment.duration(tooltipItem.xLabel, "seconds").format("*hh:mm", { trunc: true }) + ', ' + 'Rp. '+tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ') ';
       }
    }
 }
}  

class CustomerInformation extends Component {
  constructor(props) {
      super(props);

      this.state = {
        id: this.props.match.params.id ? this.props.match.params.id : '',
        customerInformation: {
            'account_number': '',
            'card_number': '',
            'start_date': '',
            'end_date': '',
            'customer_name': '',
            'phone_number': '',
        },
        isCustomerInformationLoading: false,
        query1: [],
        isQuery1Loading: false,
        query2: bar,
        isQuery2Loading: false,
        query3: bar,
        isQuery3Loading: false,
        query4: clustering_transaction_tunai,
        isQuery4Loading: false,
        query5: clustering_transaction_tunai,
        isQuery5Loading: false,
        query6: [],
        isQuery6Loading: false,
        query7: [],
        isQuery7Loading: false,
        query8: [],
        isQuery8Loading: false,
        query9: [],
        isQuery9Loading: false,
        query10: pie,
        isQuery10Loading: false,
        query11: pie,
        isQuery11Loading: false,
        query12: [],
        isQuery12Loading: false,
      };
  }

  componentDidMount() {
      this.getAllData();
  }

  getAllData() {
    this.setState({
        isCustomerInformationLoading: true,
        isQuery1Loading: true,
        isQuery2Loading: true,
        isQuery3Loading: true,
        isQuery4Loading: true,
        isQuery5Loading: true,
        isQuery6Loading: true,
        isQuery7Loading: true,
        isQuery8Loading: true,
        isQuery9Loading: true,
        isQuery10Loading: true,
        isQuery11Loading: true,
        isQuery12Loading: true,
    });

    callAPI('http://localhost:5000/customer-profile/' + this.state.id, (response) => {
        const data = JSON.parse(response[0]['result']);
        let customerInformation = {...this.state.customerInformation};
        customerInformation['account_number'] = response[0]['norek'];
        customerInformation['start_date'] = response[0]['start_date'];
        customerInformation['end_date'] = response[0]['end_date'];

        this.setState({
            customerInformation,
            isCustomerInformationLoading: false,
        });

        this.setQuery1(data.query1);
        this.setQuery2(data.query2);
        this.setQuery3(data.query3);
        this.setQuery4(data.query4);
        this.setQuery5(data.query5);
        this.setQuery6(data.query6);
        this.setQuery7(data.query7);
        this.setQuery8(data.query8);
        this.setQuery9(data.query9);
        this.setQuery10(data.query10);
        this.setQuery11(data.query11);
        this.setQuery12(data.query12);
    }, 'GET');
  }

  setQuery1(data) {
    this.setState({
        query1: data,
        isQuery1Loading: false,
    });
  }

  setQuery2(data) {
    var bar = {        
        labels: [],
        datasets: [
        {
            label: 'Frequency',
            backgroundColor: 'rgba(32, 168, 216,0.2)',
            borderColor: 'rgba(32, 168, 216,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(32, 168, 216,0.4)',
            hoverBorderColor: 'rgba(32, 168, 216,1)',
            data: [],
        },
        ]
    }

    for (let i in data) {
        bar.labels.push(data[i]['amount']);
        bar.datasets[0].data.push(data[i]['frekuensi']);
    }

    this.setState({
        query2: bar,
        isQuery2Loading: false,
    });
  }

  setQuery3(data) {
    var bar = {        
        labels: [],
        datasets: [
        {
            label: 'Frequency',
            backgroundColor: 'rgba(32, 168, 216,0.2)',
            borderColor: 'rgba(32, 168, 216,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(32, 168, 216,0.4)',
            hoverBorderColor: 'rgba(32, 168, 216,1)',
            data: [],
        },
        ]
    }

    for (let i in data) {
        bar.labels.push(data[i]['time']);
        bar.datasets[0].data.push(data[i]['frequency']);
    }

    this.setState({
        query3: bar,
        isQuery3Loading: false,
    });
  }

  setQuery4(data) {
      let scatter = {
        labels: ['Clustering Transaction Tunai'],
        datasets: [
          {
            label: 'INTERNET BANKING',
            backgroundColor: '#38f5be',
            borderColor: '#38f5be',
            borderWidth: 1,
            hoverBackgroundColor: '#38f5be',
            hoverBorderColor: '#38f5be',
            data: [],
          },
          {
            label: 'SMS BANKING',
            backgroundColor: '#997af8',
            borderColor: '#997af8',
            borderWidth: 1,
            hoverBackgroundColor: '#997af8',
            hoverBorderColor: '#997af8',
            data: [],
          },
          {
            label: 'EDC',
            backgroundColor: '#293249',
            borderColor: '#293249',
            borderWidth: 1,
            hoverBackgroundColor: '#293249',
            hoverBorderColor: '#293249',
            data: [],
          },
          {
            label: 'ATM',
            backgroundColor: '#f47f42',
            borderColor: '#f47f42',
            borderWidth: 1,
            hoverBackgroundColor: '#f47f42',
            hoverBorderColor: '#f47f42',
            data: [],
          }
        ]
      };
      for (let i in data) {
          if (data[i]['tipe_channel'] === 'INTERNET BANKING') {
            scatter['datasets'][0]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          } else if (data[i]['tipe_channel'] === 'SMS BANKING') {
            scatter['datasets'][1]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          } else if (data[i]['tipe_channel'] === 'EDC') {
            scatter['datasets'][2]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          } else if (data[i]['tipe_channel'] === 'ATM') {
            scatter['datasets'][3]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          }
      }

      this.setState({
        query4: scatter,
        isQuery4Loading: false,
      });
  }

  setQuery5(data) {
      let scatter = {
        labels: ['Clustering Transaction Tunai'],
        datasets: [
          {
            label: 'INTERNET BANKING',
            backgroundColor: '#38f5be',
            borderColor: '#38f5be',
            borderWidth: 1,
            hoverBackgroundColor: '#38f5be',
            hoverBorderColor: '#38f5be',
            data: [],
          },
          {
            label: 'SMS BANKING',
            backgroundColor: '#997af8',
            borderColor: '#997af8',
            borderWidth: 1,
            hoverBackgroundColor: '#997af8',
            hoverBorderColor: '#997af8',
            data: [],
          },
          {
            label: 'EDC',
            backgroundColor: '#293249',
            borderColor: '#293249',
            borderWidth: 1,
            hoverBackgroundColor: '#293249',
            hoverBorderColor: '#293249',
            data: [],
          },
          {
            label: 'ATM',
            backgroundColor: '#f47f42',
            borderColor: '#f47f42',
            borderWidth: 1,
            hoverBackgroundColor: '#f47f42',
            hoverBorderColor: '#f47f42',
            data: [],
          }
        ]
      };

      for (let i in data) {
          if (data[i]['tipe_channel'] === 'INTERNET BANKING') {
            scatter['datasets'][0]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          } else if (data[i]['tipe_channel'] === 'SMS BANKING') {
            scatter['datasets'][1]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          } else if (data[i]['tipe_channel'] === 'EDC') {
            scatter['datasets'][2]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          } else if (data[i]['tipe_channel'] === 'ATM') {
            scatter['datasets'][3]['data'].push({x: data[i]['detik'], y: data[i]['trx_amount']});
          }
      }

      this.setState({
        query5: scatter,
        isQuery5Loading: false,
      });
  }

  setQuery6(data) {
    this.setState({
        query6: data,
        isQuery6Loading: false,
    });
  }

  setQuery7(data) {
    this.setState({
        query7: data,
        isQuery7Loading: false,
    });
  }

  setQuery8(data) {
    this.setState({
        query8: data,
        isQuery8Loading: false,
    });
  }

  setQuery9(data) {
    this.setState({
        query9: data,
        isQuery9Loading: false,
    });
  }

  setQuery10(data) {
    let pie = {
        labels: [],
        datasets: [
          {
            data: [],
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#4dbd74',
            ],
            hoverBackgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#4dbd74',
            ],
          }],
      };

    for (let i in data) {
        pie.labels.push(data[i]['channel']);
        pie.datasets[0].data.push(data[i]['channel_amount']);
    }

    this.setState({
        query10: pie,
        isQuery10Loading: false,
    });
  }

  setQuery11(data) {
    let pie = {
        labels: [],
        datasets: [
          {
            data: [],
            backgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#4dbd74',
            ],
            hoverBackgroundColor: [
              '#FF6384',
              '#36A2EB',
              '#FFCE56',
              '#4dbd74',
            ],
          }],
      };

    for (let i in data) {
        pie.labels.push(data[i]['tipe_channel']);
        pie.datasets[0].data.push(data[i]['frequency']);
    }

    this.setState({
        query11: pie,
        isQuery11Loading: false,
    });
  }

  setQuery12(data) {
    this.setState({
        query12: data,
        isQuery12Loading: false,
    });
  }

  render() {      
    return (
      <div className="animated fadeIn">
          <Row>
              <Col>
                  <Button outline color="primary" className="float-right p-2 m-3"><i className="fa fa-file-pdf-o mr-1"></i> Download Report AS PDF</Button>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Customer Information</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Loading show={this.state.isCustomerInformationLoading}>
                            <Row>
                                <dt className="col-sm-3">Account Number</dt>
                                <dd className="col-sm-9">{this.state.customerInformation.account_number}</dd>
                            </Row>
                            <Row>
                                <dt className="col-sm-3">Card Number</dt>
                                <dd className="col-sm-9">-</dd>
                            </Row>
                            <Row>
                                <dt className="col-sm-3">Time Period</dt>
                                <dd className="col-sm-9">{Moment(this.state.customerInformation.start_date).format('DD-MM-YYYY')} - {Moment(this.state.customerInformation.end_date).format('DD-MM-YYYY')}</dd>
                            </Row>
                            <Row>
                                <dt className="col-sm-3">Customer Name</dt>
                                <dd className="col-sm-9">-</dd>
                            </Row>
                            <Row>
                                <dt className="col-sm-3">Phone Number</dt>
                                <dd className="col-sm-9">-</dd>
                            </Row>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                  <Card>
                      <CardBody>
                        <Row>
                            <Col xs="12">
                                <Button outline color="primary" className="float-right mb-3"><i className="fa fa-file-excel-o mr-1"></i> Download Transaction as CSV</Button>
                                <h4>Customer Transactions</h4>
                            </Col>
                        </Row>                                
                        <Row>
                            <Col>
                                <Loading show={this.state.isQuery1Loading} >
                                    <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                        <thead className="thead-light">
                                            <tr>
                                                <th>Card Number</th>
                                                <th>Amount</th>
                                                <th>Channel</th>
                                                <th>Transaction Time</th>
                                                <th>Activity</th>
                                                <th>Location</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                        
                                        { this.state.query1.map((row, index) => {
                                            return (
                                            <tr key={index}>
                                                <td>
                                                <div>{row.norek}</div>
                                                    <div className="small text-muted">
                                                        <span>Card Number Hash</span> 000000
                                                    </div>    
                                                </td>
                                                <td>Rp. {row.trx_amount}</td>
                                                <td>
                                                    <div>{row.tipe_channel}</div>
                                                    <div className="small text-muted">
                                                        <span>No.</span> 000000
                                                    </div>    
                                                </td>
                                                <td>
                                                    <div>{row.timestamp}</div>
                                                    <div className="small text-muted">
                                                        <span>Time</span> 00:00:00
                                                    </div>
                                                </td>
                                                <td>{row.jenis_trx}</td>
                                                <td></td>
                                                <td>                                            
                                                    <div>{row.status_code}</div>
                                                    <div className="small text-muted">
                                                        <span>Core 12</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            )
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </Loading>
                            </Col>
                        </Row>
                      </CardBody>
                  </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        Flow Transaction
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Transaction Frequency by Nominal</h4>
                            </Col>
                        </Row>
                        <hr />
                        <div className="chart-wrapper">                        
                            <Loading show={this.state.isQuery2Loading} >
                                <Bar data={this.state.query2} options={options} />
                            </Loading>
                        </div>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Transaction Frequency by Time</h4>
                            </Col>
                        </Row>
                        <hr />
                        <div className="chart-wrapper">
                            <Loading show={this.state.isQuery3Loading} >
                                <Bar data={this.state.query3} options={options} />
                            </Loading>
                        </div>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Tunai Transaction Clustering by Channel</h4>
                            </Col>
                        </Row>
                        <hr />
                        <div className="chart-wrapper">
                            <Loading show={this.state.isQuery4Loading} >
                                <Scatter data={this.state.query4} options={clustering_transaction} />
                            </Loading>
                        </div>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Non Tunai Transaction Clustering by Channel</h4>
                            </Col>
                        </Row>
                        <hr />
                        <div className="chart-wrapper">
                            <Loading show={this.state.isQuery5Loading} >
                                <Scatter data={this.state.query5}  options={clustering_transaction}/>
                            </Loading>
                        </div>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Top Tunai Activity by Amount</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Loading show={this.state.isQuery6Loading}>
                            <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">No</th>
                                        <th className="text-center">Activity</th>
                                        <th className="text-center">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>                                                                   
                                { this.state.query6.map((row, index) => {
                                    return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{row.jenis_trx}</td>
                                        <td>Rp. {row.amount}</td>
                                    </tr>
                                    )
                                    })
                                }
                                </tbody>
                            </Table>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Top Tunai Activity by Frequency</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Loading show={this.state.isQuery7Loading} >
                            <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">No</th>
                                        <th className="text-center">Activity</th>
                                        <th className="text-center">Frequency</th>
                                    </tr>
                                </thead>
                                <tbody>                                                              
                                    { this.state.query7.map((row, index) => {
                                        return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{row.jenis_trx}</td>
                                            <td>{row.frequency}</td>
                                        </tr>
                                        )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Top Non Tunai Activity by Amount</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Loading show={this.state.isQuery8Loading} >
                            <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">No</th>
                                        <th className="text-center">Activity</th>
                                        <th className="text-center">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>                                            
                                    { this.state.query8.map((row, index) => {
                                        return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{row.jenis_trx}</td>
                                            <td>Rp. {row.jumlah}</td>
                                        </tr>
                                        )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Top Non Tunai Activity by Frequency</h4>
                            </Col>
                        </Row>
                        <hr />
                        <Loading show={this.state.isQuery9Loading} >
                            <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">No</th>
                                        <th className="text-center">Activity</th>
                                        <th className="text-center">Frequency</th>
                                    </tr>
                                </thead>
                                <tbody>                                            
                                    { this.state.query9.map((row, index) => {
                                        return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{row.non_tunai}</td>
                                            <td>{row.non_tunai_frequency}</td>
                                        </tr>
                                        )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col xs="6">
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Transaction Amount by Channel</h4>
                            </Col>
                        </Row>
                        <hr />
                        <div className="chart-wrapper">
                            <Loading show={this.state.isQuery10Loading} >
                                <Pie data={this.state.query10} />
                            </Loading>
                        </div>
                    </CardBody>
                </Card>
              </Col>
              <Col xs="6">
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Transaction Frequency by Channel</h4>
                            </Col>
                        </Row>
                        <hr />
                        <div className="chart-wrapper">
                            <Loading show={this.state.isQuery11Loading} >
                                <Pie data={this.state.query11} />
                            </Loading>
                        </div>
                    </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
              <Col>
                <Card>
                    <CardBody>
                        <Row>
                            <Col xs="12">
                                <h4>Summary of Transaction by Channel</h4>
                            </Col>
                        </Row>
                        <hr />                        
                        <Loading show={this.state.isQuery12Loading} >
                            <Table hover responsive className="table-outline mb-2 d-none d-sm-table">
                                <thead className="thead-light">
                                    <tr>
                                        <th className="text-center">Channel</th>
                                        <th className="text-center">Minimum Amount</th>
                                        <th className="text-center">Maximum Amount</th>
                                        <th className="text-center">Median Amount</th>
                                    </tr>
                                </thead>
                                <tbody>                                                            
                                    { this.state.query12.map((row, index) => {
                                        return (
                                        <tr key={index}>
                                            <td>{row.tipe_channel}</td>
                                            <td>{row.min}</td>
                                            <td>{row.median}</td>
                                            <td>{row.max}</td>
                                        </tr>
                                        )
                                        })
                                    }
                                </tbody>
                            </Table>
                        </Loading>
                    </CardBody>
                </Card>
              </Col>
          </Row>
      </div>
    );
  }
}

export default CustomerInformation;
