import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

// const Dashboard = React.lazy(() => import('./views/Dashboard'));
// const Table = React.lazy(() => import('./views/TableExample'));
// const Download = React.lazy(() => import('./views/Download'));
// const Customers = React.lazy(() => import('./views/Customers'));
// const CustomerInformation = React.lazy(() => import('./views/CustomerInformation'));
const User = React.lazy(() => import('./views/Users/User'));
const CreateUser = React.lazy(() => import('./views/CreateUser/CreateUser'));
// const UpdateUser = React.lazy(() => import('./views/UpdateUser/UpdateUser'));
// const Roles = React.lazy(() => import('./views/Roles/Roles'));
// const CreateRole = React.lazy(() => import('./views/Roles/CreateRole'));
// const UpdateRole = React.lazy(() => import('./views/Roles/UpdateRole'));
// const DashboardNasabah = React.lazy(() => import('./views/DashboardNasabah/DashboardNasabah'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  // { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  // { path: '/table', name: 'Table', component: Table },
  // { path: '/download', name: 'Download', component: Download },
  // { path: '/customers', name: 'Download', component: Customers },
  // { path: '/customer-information/:id', name: 'Customer Information', component: CustomerInformation },
  { path: '/user', name: 'User', component: User },
  { path: '/create-user', name: 'Create User', component: CreateUser },
  // { path: '/update-user/:id', name: 'Update User', component: UpdateUser },
  // { path: '/roles', name: 'Roles', component: Roles },
  // { path: '/create-role', name: 'Create Role', component: CreateRole },
  // { path: '/update-role/:id', name: 'Update Role', component: UpdateRole },
  // { path: '/dashboard-nasabah', name: 'Dashboard Nasabah', component: DashboardNasabah },
];

export default routes;
